package de.tadris.flang_lib

enum class Type(val c: Char,
                val value: Int,
                val moves: Array<Array<Vector>>,
                val hasFreeze: Boolean = true,
                val hasDoubleMoves: Boolean = false) {

    KING('k', 4, arrayOf(
        arrayOf(Vector(-1, -1)),
        arrayOf(Vector(-1, 0)),
        arrayOf(Vector(-1, 1)),

        arrayOf(Vector(0, -1)),
        arrayOf(Vector(0, 1)),

        arrayOf(Vector(1, -1)),
        arrayOf(Vector(1, 0)),
        arrayOf(Vector(1, 1)),
    ), false),

    PAWN('p', 1, arrayOf(
        arrayOf(Vector(-1, 1)),
        arrayOf(Vector(0, 1)),
        arrayOf(Vector(1, 1)),
    )),

    HORSE('h', 2, arrayOf(
        arrayOf(Vector(-1, 2)),
        arrayOf(Vector(-1, -2)),

        arrayOf(Vector(-2, 1)),
        arrayOf(Vector(-2, -1)),

        arrayOf(Vector(1, 2)),
        arrayOf(Vector(1, -2)),

        arrayOf(Vector(2, -1)),
        arrayOf(Vector(2, 1)),
    )),

    ROOK('r', 4, arrayOf(
        arrayOf(
            Vector(0, 1),
            Vector(0, 2),
            Vector(0, 3),
            Vector(0, 4),
            Vector(0, 5),
            Vector(0, 6),
            Vector(0, 7),
        ),
        arrayOf(
            Vector(0, -1),
            Vector(0, -2),
            Vector(0, -3),
            Vector(0, -4),
            Vector(0, -5),
            Vector(0, -6),
            Vector(0, -7),
        ),
        arrayOf(
            Vector(1, 0),
            Vector(2, 0),
            Vector(3, 0),
            Vector(4, 0),
            Vector(5, 0),
            Vector(6, 0),
            Vector(7, 0),
        ),
        arrayOf(
            Vector(-1, 0),
            Vector(-2, 0),
            Vector(-3, 0),
            Vector(-4, 0),
            Vector(-5, 0),
            Vector(-6, 0),
            Vector(-7, 0),
        ),
    )),

    UNI('u', 9, arrayOf(
        arrayOf(
            Vector(0, 1),
            Vector(0, 2),
            Vector(0, 3),
            Vector(0, 4),
            Vector(0, 5),
            Vector(0, 6),
            Vector(0, 7),
        ),
        arrayOf(
            Vector(0, -1),
            Vector(0, -2),
            Vector(0, -3),
            Vector(0, -4),
            Vector(0, -5),
            Vector(0, -6),
            Vector(0, -7),
        ),
        arrayOf(
            Vector(1, 0),
            Vector(2, 0),
            Vector(3, 0),
            Vector(4, 0),
            Vector(5, 0),
            Vector(6, 0),
            Vector(7, 0),
        ),
        arrayOf(
            Vector(-1, 0),
            Vector(-2, 0),
            Vector(-3, 0),
            Vector(-4, 0),
            Vector(-5, 0),
            Vector(-6, 0),
            Vector(-7, 0),
        ),
        arrayOf(Vector(-1, 2)),
        arrayOf(Vector(-1, -2)),

        arrayOf(Vector(-2, 1)),
        arrayOf(Vector(-2, -1)),

        arrayOf(Vector(1, 2)),
        arrayOf(Vector(1, -2)),

        arrayOf(Vector(2, -1)),
        arrayOf(Vector(2, 1)),

        arrayOf(
            Vector(-1, -1),
            Vector(-2, -2),
            Vector(-3, -3),
            Vector(-4, -4),
            Vector(-5, -5),
            Vector(-6, -6),
            Vector(-7, -7),
        ),

        arrayOf(
            Vector(1, 1),
            Vector(2, 2),
            Vector(3, 3),
            Vector(4, 4),
            Vector(5, 5),
            Vector(6, 6),
            Vector(7, 7),
        ),

        arrayOf(
            Vector(1, -1),
            Vector(2, -2),
            Vector(3, -3),
            Vector(4, -4),
            Vector(5, -5),
            Vector(6, -6),
            Vector(7, -7),
        ),

        arrayOf(
            Vector(-1, 1),
            Vector(-2, 2),
            Vector(-3, 3),
            Vector(-4, 4),
            Vector(-5, 5),
            Vector(-6, 6),
            Vector(-7, 7),
        ),
    )),

    FLANGER('f', 7, arrayOf(
        arrayOf(
            Vector(-1, 1),
            Vector(-2, 0),
            Vector(-3, 1),
            Vector(-4, 0),
            Vector(-5, 1),
            Vector(-6, 0),
            Vector(-7, 1),
        ),
        arrayOf(
            Vector(-1, -1),
            Vector(-2, 0),
            Vector(-3, -1),
            Vector(-4, 0),
            Vector(-5, -1),
            Vector(-6, 0),
            Vector(-7, -1),
        ),
        arrayOf(
            Vector(1, 1),
            Vector(2, 0),
            Vector(3, 1),
            Vector(4, 0),
            Vector(5, 1),
            Vector(6, 0),
            Vector(7, 1),
        ),
        arrayOf(
            Vector(1, -1),
            Vector(2, 0),
            Vector(3, -1),
            Vector(4, 0),
            Vector(5, -1),
            Vector(6, 0),
            Vector(7, -1),
        ),

        arrayOf(
            Vector(1, -1),
            Vector(0, -2),
            Vector(1, -3),
            Vector(0, -4),
            Vector(1, -5),
            Vector(0, -6),
            Vector(1, -7),
        ),
        arrayOf(
            Vector(-1, -1),
            Vector(0, -2),
            Vector(-1, -3),
            Vector(0, -4),
            Vector(-1, -5),
            Vector(0, -6),
            Vector(-1, -7),
        ),
        arrayOf(
            Vector(1, 1),
            Vector(0, 2),
            Vector(1, 3),
            Vector(0, 4),
            Vector(1, 5),
            Vector(0, 6),
            Vector(1, 7),
        ),
        arrayOf(
            Vector(-1, 1),
            Vector(0, 2),
            Vector(-1, 3),
            Vector(0, 4),
            Vector(-1, 5),
            Vector(0, 6),
            Vector(-1, 7),
        ),
    ), hasDoubleMoves = true);

    fun getChar(color: Color): Char {
        return if(color == Color.WHITE) c.toUpperCase() else c
    }

    companion object {

        val _values by lazy { values() }

        fun getType(char: Char) = getTypeOrNull(char) ?: throw IllegalArgumentException("Type '$char' is not known.")

        fun getTypeOrNull(char: Char) = _values.find { it.c == char.toLowerCase() }

    }

}