package de.tadris.flang_lib.analysis

class AnalysisResult(val moveEvaluations: List<AnalysisMoveEvaluation>,
                     val white: ColorAnalysis,
                     val black: ColorAnalysis)