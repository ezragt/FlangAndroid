package de.tadris.flang_lib.analysis

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Action
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.action.Resign
import de.tadris.flang_lib.bot.NeoBoardEvaluation
import de.tadris.flang_lib.bot.FlangBot
import java.lang.Exception
import java.util.concurrent.Executors
import kotlin.math.absoluteValue

class ComputerAnalysis(
    fmn: String,
    private val depth: Int,
    private val listener: AnalysisListener) {

    private val fullHistory = Board.fromFMN(fmn).moveList
    private val actionList = ActionList()
    private val moveEvaluations = arrayOfNulls<AnalysisMoveEvaluation>(fullHistory.size)

    private val lock = Object()
    private var active = 0
    private var finished = 0

    private val executors = Executors.newFixedThreadPool(8)

    fun analyze(): AnalysisResult {
        fullHistory.actions.forEachIndexed { index, action ->
            val board = actionList.board.clone(true)
            actionList.addMove(action)
            synchronized(lock){
                active++
            }
            executors.submit {
                try {
                    analyzeAction(index, board, action)
                }catch (e: Exception){
                    e.printStackTrace()
                } finally {
                    synchronized(lock){
                        active--
                        finished++
                    }
                }
            }
        }
        while (active > 0){
            println("Running... $active active")
            listener.onProgressChanged(finished * 100 / fullHistory.size)
            Thread.sleep(500)
        }
        executors.shutdown()
        return AnalysisResult(moveEvaluations.toList().filterNotNull(),
            analyzeColor(Color.WHITE),
            analyzeColor(Color.BLACK))
    }

    private fun analyzeColor(color: Color): ColorAnalysis {
        val moves = moveEvaluations.filterNotNull().filter { it.isWhite.xor(color == Color.BLACK) }
        if(moves.isEmpty()) return ColorAnalysis(0.0)
        val accuracy = moves.sumOf { it.score } / moves.size
        return ColorAnalysis(accuracy)
    }

    private fun analyzeAction(index: Int, board: Board, action: Action){
        when(action){
            is Move -> analyzeMove(index, board, action)
            is Resign -> analyzeResign(index, board, action)
        }
    }

    private fun analyzeMove(index: Int, board: Board, move: Move){
        val result = createFlangBot().findBestMove(board)
        val previousEval = NeoBoardEvaluation(board).evaluate()

        val targetMoveEval = result.evaluations.find { move.getNotation() == it.move.getNotation() }?.evaluation
        if(targetMoveEval == null){
            println("Cannot analyze! Move $move was not found in the bot result for $board")
            println("Possible moves: " + result.evaluations.map { it.move.getNotation() })
            return
        }
        val bestMove = result.evaluations.first()
        val bestMoveEval = bestMove.evaluation
        val worstMoveEval = result.evaluations.lastOrNull { it.evaluation.absoluteValue < 100 }?.evaluation
            ?: result.evaluations.last().evaluation
        val moveScore = if(bestMoveEval == worstMoveEval){
            1.0
        }else{
            ((targetMoveEval - worstMoveEval) / (bestMoveEval - worstMoveEval))
                .coerceIn(0.0, 1.0)
        }

        board.executeOnBoard(move)
        val nextEval = NeoBoardEvaluation(board).evaluate()
        val evalDiff = nextEval - previousEval

        val weight = (evalDiff.absoluteValue / 5).coerceIn(0.0, 1.0)
        val type = when {
            moveScore < 0.5 && weight > 0.5 -> AnalysisMoveType.BLUNDER
            moveScore < 0.5 -> AnalysisMoveType.MISTAKE
            else -> AnalysisMoveType.GOOD
        }

        moveEvaluations[index] = AnalysisMoveEvaluation(move.getNotation(), bestMove.move.getNotation(), move.piece.color == Color.WHITE, moveScore, nextEval, bestMoveEval, weight, type)
    }

    private fun analyzeResign(index: Int, board: Board, resign: Resign){
        resign.applyToBoard(board)
        val nextEval = NeoBoardEvaluation(board).evaluate()
        moveEvaluations[index] = AnalysisMoveEvaluation(resign.toString(), "", resign.color == Color.WHITE, 0.0, nextEval, nextEval, 1.0, AnalysisMoveType.RESIGN)
    }

    private fun createFlangBot() = FlangBot(depth, depth)

}