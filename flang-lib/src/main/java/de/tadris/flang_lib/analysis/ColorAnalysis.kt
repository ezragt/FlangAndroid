package de.tadris.flang_lib.analysis

class ColorAnalysis(val accuracy: Double){

    override fun toString(): String {
        return "accuracy=$accuracy"
    }
}