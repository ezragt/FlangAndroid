package de.tadris.flang_lib.bot

import de.tadris.flang_lib.*
import de.tadris.flang_lib.action.Move

typealias MoveAction = (x: Int, y: Int) -> Unit

class FastMoveGenerator(var board: Board, val includeOwnPieces: Boolean, val kingRange: Int = 1) {

    inline fun forEachMove(color: Color?, action: (Move) -> Unit){
        if(board.gameIsComplete()){
            return
        }
        board.eachPiece(color){
            forEachMove(it, action)
        }
    }

    inline fun forEachMove(piece: Piece, action: (Move) -> Unit){
        forEachTargetLocation(piece.location.x, piece.location.y, piece.color, piece.type, piece.state){ x, y ->
            action(Move(piece, Location(board, x, y)))
        }
    }

    // For REAL target locations use the slower piece.getTargets()
    // This returns a greater influence for the king
    // (2 moves for the king because the king moves twice as fast)
    inline fun forEachTargetLocation(x: Int, y: Int, color: Color, type: Type, state: PieceState, action: MoveAction) {
        if(state == PieceState.FROZEN){
            return
        }
        when(type){
            Type.PAWN -> forEachMoveForPawn(x, y, color, action)
            Type.KING -> forEachKingMove(x, y, color, action)
            else -> forEachPossibleTargetLocation(x, y, type, color, action)
        }
    }

    inline fun forEachMoveForPawn(x: Int, y: Int, color: Color, action: MoveAction) {
        // Optimized method for pawns

        val yDirection = color.evaluationNumber

        if(checkTarget(x, y + yDirection, color)){
            action(x, y + yDirection)
        }
        if(checkTarget(x + 1, y + yDirection, color)){
            action(x + 1, y + yDirection)
        }
        if(checkTarget(x - 1, y + yDirection, color)){
            action(x - 1, y + yDirection)
        }

    }

    inline fun forEachKingMove(x: Int, y: Int, color: Color, action: MoveAction) {
        // Optimized method for kings

        for(dx in -kingRange..kingRange){
            for(dy in -kingRange..kingRange){
                if(dx == 0 && dy == 0) continue
                if(checkTarget(x + dx, y + dy, color)){
                    action(x + dx, y + dy)
                }
            }
        }
    }

    val targets = mutableListOf<Int>()
    inline fun forEachPossibleTargetLocation(x: Int, y: Int, type: Type, color: Color, action: MoveAction) {
        if(type.hasDoubleMoves) targets.clear()

        type.moves.forEach { batch ->
            forEachPossibleTargetLocation(x, y, color, batch) { x, y ->
                val hashCode = y * Board.BOARD_SIZE + x
                if(!type.hasDoubleMoves || !targets.contains(hashCode)){
                    if(type.hasDoubleMoves)
                        targets.add(hashCode)
                    action(x, y)
                }
            }
        }
    }

    inline fun forEachPossibleTargetLocation(pieceX: Int, pieceY: Int, color: Color, batch: Array<Vector>, action: MoveAction) {
        batch.forEach {
            val x = pieceX + it.x
            val y = pieceY + it.y
            if(checkTarget(x, y, color)){
                action(x, y)
                if(!isEmpty(x, y)){
                    return
                }
            }else{
                return
            }
        }
    }

    fun checkTarget(x: Int, y: Int, color: Color): Boolean {
        return isValid(x, y) && (includeOwnPieces || isEmpty(x, y) || (isWhite(x, y) != (color == Color.WHITE)))
    }

    fun isEmpty(x: Int, y: Int): Boolean {
        return getChar(x, y) == Board.EMPTY
    }

    fun isWhite(x: Int, y: Int): Boolean{
        return getChar(x, y).isUpperCase()
    }

    fun getChar(x: Int, y: Int): Char {
        return board.pieces[(y * Board.BOARD_SIZE + x) * 2]
    }

    fun isValid(x: Int, y: Int) = x >= 0 && y >= 0 && x < Board.BOARD_SIZE && y < Board.BOARD_SIZE

}