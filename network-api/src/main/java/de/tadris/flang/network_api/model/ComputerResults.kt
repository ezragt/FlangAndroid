package de.tadris.flang.network_api.model

data class ComputerResults(val results: List<ComputerResult>)
