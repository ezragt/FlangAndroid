package de.tadris.flang.network_api.exception

open class FlangAPIException(message: String) : Exception("API-Exception - $message")