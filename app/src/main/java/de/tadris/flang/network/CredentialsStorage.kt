package de.tadris.flang.network

import android.content.Context
import de.tadris.flang.network_api.model.Session

class CredentialsStorage(context: Context) {

    private val prefs = context.getSharedPreferences("credentials", Context.MODE_PRIVATE)

    fun getUsername() = prefs.getString("username", "")!!

    fun getSessionKey() = prefs.getString("key", "")!!

    fun saveSession(username: String, session: Session){
        prefs.edit().putString("username", username).putString("key", session.key).apply()
    }

    fun clear(){
        prefs.edit().clear().apply()
    }

}