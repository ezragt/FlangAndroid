package de.tadris.flang.game

import android.app.Activity
import de.tadris.flang.network_api.model.GameConfiguration
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.bot.FlangBot
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.action.Resign
import kotlin.concurrent.thread

open class OfflineBotGameController(activity: Activity) : AbstractGameController(activity) {

    companion object {
        const val NAME = "Flangbot Classic"
        const val EASY_STRENGTH = 2
        const val DEFAULT_STRENGTH = 4
    }

    protected var color = Color.WHITE // Color of user
    protected var board = Board(Board.DEFAULT_BOARD)
    protected var strength = DEFAULT_STRENGTH

    fun updateStrength(strength: Int){
        this.strength = strength
        updateGameState()
    }

    override fun onMoveRequested(move: Move, newBoardRequest: Board?) {
        if(newBoardRequest != null){
            // Accept new board
            board = newBoardRequest
        }
        board.executeOnBoard(move)
        callback.onUpdate(move)
        botTurn()
    }

    override fun requestGame() {
        color = if(Math.random() > 0.5) Color.WHITE else Color.BLACK
        updateGameState()
        if(color == Color.BLACK){
            botTurn()
        }
    }

    private fun updateGameState(){
        val player1Info = GamePlayerInfo("Player1", 0f, -1, 0f, false, "")
        val player2Info = GamePlayerInfo("Flangbot Classic#$strength", 0f, -1, 0f, true, "")
        val whiteInfo = if(color == Color.WHITE) player1Info else player2Info
        val blackInfo = if(color == Color.WHITE) player2Info else player1Info
        callback.onGameRequestSuccess(GameInfo(-1, whiteInfo, blackInfo, "", 0, running = true, GameConfiguration(
            isRated = false,
            infiniteTime = true,
            time = 0,
            isBotRequest = false
        ), lastAction = 0, won = 0, spectatorCount = 0), true, color, board.clone(true))
    }

    private fun botTurn(){
        if(board.gameIsComplete()){
            return
        }
        thread {
            val bot = FlangBot(strength, strength)
            val botMoves = bot.findBestMove(board)
            println(botMoves.evaluations)
            println("Evaluations: " + bot.evaluations)
            val botMove = botMoves.bestMove.move
            board.executeOnBoard(botMove)
            activity.runOnUiThread {
                callback.onUpdate(botMove)
            }
        }
    }

    override fun resignGame() {
        callback.onUpdate(Resign(color))
    }

    override fun isCreativeGame() = true
}