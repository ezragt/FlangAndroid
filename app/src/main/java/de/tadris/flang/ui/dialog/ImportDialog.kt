package de.tadris.flang.ui.dialog

import android.content.ClipboardManager
import android.content.Context
import android.provider.ContactsContract
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import de.tadris.flang.R
import de.tadris.flang_lib.Board
import java.lang.Exception

typealias ImportListener = (gameString: String, board: Board, type: ImportType) -> Unit

fun Fragment.openImportDialog(onImport: ImportListener){
    val editText = EditText(requireContext())
    editText.setText(readFromClipboard())
    AlertDialog.Builder(requireActivity())
        .setTitle(R.string.importGame)
        .setView(editText)
        .setPositiveButton(R.string.okay) { _, _ ->
            try{
                importAndShowGame(editText.text.toString().trim(), onImport)
            }catch (e: Exception){
                AlertDialog.Builder(requireActivity())
                    .setTitle(R.string.importFailed)
                    .setMessage(e.message)
                    .setPositiveButton(R.string.okay, null)
                    .show()
            }
        }
        .show()
}

fun Fragment.readFromClipboard(): String {
    val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager? ?: return ""
    return clipboard.primaryClip?.getItemAt(0)?.text?.toString() ?: ""
}

private fun importAndShowGame(gameString: String, onImport: ImportListener){
    if(gameString.startsWith("+") || gameString.startsWith("-")){
        onImport(gameString, Board.fromFBN2(gameString), ImportType.FBN2)
    }else{
        onImport(gameString, Board.fromFMN(gameString), ImportType.FMN)
    }
}

enum class ImportType {
    FBN2,
    FMN
}