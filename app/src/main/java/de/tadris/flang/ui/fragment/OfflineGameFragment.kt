package de.tadris.flang.ui.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import de.tadris.flang.R
import de.tadris.flang.game.GameController
import de.tadris.flang.game.OfflineBotGameController
import de.tadris.flang.game.OnlineGameController

class OfflineGameFragment : GameFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)

        root.findViewById<View>(R.id.player1InfoParent).setOnClickListener {
            openStrengthChooseDialog()
        }
        root.findViewById<View>(R.id.player2InfoParent).setOnClickListener {
            openStrengthChooseDialog()
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        openStrengthChooseDialog()
    }

    private fun openStrengthChooseDialog(){
        val arrayAdapter = ArrayAdapter<String>(requireActivity(), android.R.layout.select_dialog_item)
        val options = mutableMapOf(
            OfflineBotGameController.NAME + "#0" to 0,
            OfflineBotGameController.NAME + "#2" to 2,
            OfflineBotGameController.NAME + "#3" to 3,
            OfflineBotGameController.NAME + "#4" to 4,
        )
        arrayAdapter.addAll(options.keys)
        AlertDialog.Builder(activity)
            .setTitle(R.string.offlineChooseStrength)
            .setAdapter(arrayAdapter) { _, which ->
                (gameController as? OfflineBotGameController)
                    ?.updateStrength(options.entries.toList()[which].value)
            }
            .show()
    }

    override fun createGameController(): GameController {
        return OfflineBotGameController(requireActivity())
    }

    override fun getNavigationLinkToAnalysis() = R.id.action_nav_offline_game_to_nav_analysis
    override fun getNavigationLinkToChat() = R.id.action_nav_offline_game_to_nav_chat
}