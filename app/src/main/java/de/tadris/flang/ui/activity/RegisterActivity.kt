package de.tadris.flang.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import de.tadris.flang.R
import de.tadris.flang.databinding.ActivityRegisterBinding
import kotlinx.coroutines.launch

class RegisterActivity : AuthActivity() {

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.registerSubmit.setOnClickListener {
            lifecycleScope.launch {
                authenticate(binding.registerUsername.text.toString(), binding.registerPassword.text.toString(), true)
            }
        }

        binding.consent1.setOnCheckedChangeListener { _, _ -> refresh() }
        binding.consent2.setOnCheckedChangeListener { _, _ -> refresh() }
        binding.consent3.setOnCheckedChangeListener { _, _ -> refresh() }
        binding.consent4.setOnCheckedChangeListener { _, _ -> refresh() }

        findViewById<Button>(R.id.registerPrivacyPolicy).setOnClickListener {
            openPrivacyPolicy()
        }
    }

    private fun refresh(){
        binding.registerSubmit.isEnabled =
                    binding.consent1.isChecked &&
                    binding.consent2.isChecked &&
                    binding.consent3.isChecked &&
                    binding.consent4.isChecked
    }

    private fun openPrivacyPolicy(){
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacyPolicyUrl)))
        startActivity(browserIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}