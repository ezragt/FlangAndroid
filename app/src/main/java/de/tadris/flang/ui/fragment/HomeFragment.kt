package de.tadris.flang.ui.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.WorkerThread
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.tadris.flang.R
import de.tadris.flang.databinding.FragmentHomeBinding
import de.tadris.flang.network.DataRepository
import de.tadris.flang.network_api.exception.NotFoundException
import de.tadris.flang.network_api.exception.UnauthorizedException
import de.tadris.flang.network_api.model.*
import de.tadris.flang.ui.activity.LoginActivity
import de.tadris.flang.ui.adapter.GameAdapter
import de.tadris.flang.ui.adapter.GameConfigurationAdapter
import de.tadris.flang.ui.adapter.ServerAnnouncementAdapter
import de.tadris.flang.ui.dialog.CustomGameConfigurationDialog
import de.tadris.flang.ui.dialog.GameRequestAcceptDialog
import de.tadris.flang.ui.dialog.GameRequestAddDialog
import de.tadris.flang.ui.view.GameRequestView
import kotlin.Exception
import kotlin.concurrent.thread

class HomeFragment : Fragment(R.layout.fragment_home), GameConfigurationAdapter.ConfigurationListener, GameRequestView.GameRequestListener,
    GameAdapter.GameAdapterListener, CustomGameConfigurationDialog.CustomGameConfigurationListener,
    ServerAnnouncementAdapter.AnnouncementAdapterCallback {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!


    private val configurationAdapter = GameConfigurationAdapter(this)
    private val announcementAdapter = ServerAnnouncementAdapter(this, emptyList())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!
        _binding = FragmentHomeBinding.bind(root)

        binding.requestConfigurations.adapter = configurationAdapter
        binding.requestConfigurations.layoutManager = GridLayoutManager(requireContext(), 3)

        binding.homeServerAnnouncements.adapter = announcementAdapter
        binding.homeServerAnnouncements.layoutManager = LinearLayoutManager(requireContext())

        binding.homeActiveGames.layoutManager = LinearLayoutManager(requireContext())

        binding.homeLearn.setOnClickListener {
            openTutorial()
        }

        checkFirstStart()

        return root
    }

    override fun onResume() {
        super.onResume()
        startRefreshThread()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun startRefreshThread(){
        thread {
            try{
                DataRepository.getInstance().login(requireContext())
            }catch (e: Exception){
                e.printStackTrace()
            }
            while (isResumed){
                refreshLobby()
                Thread.sleep(4000)
            }
        }
    }

    @WorkerThread
    private fun refreshLobby(){
        try{
            val serverInfo = DataRepository.getInstance().accessOpenAPI().getInfo()
            val lobby = DataRepository.getInstance().accessOpenAPI().getLobby()
            val activeGames = try {
                if(DataRepository.getInstance().credentialsAvailable(requireContext()))
                    DataRepository.getInstance().accessRestrictedAPI(requireContext()).findActive()
                else null
            }catch (e: UnauthorizedException){
                e.printStackTrace()
                DataRepository.getInstance().resetLogin()
                null
            }
            activity?.runOnUiThread {
                showInfo(serverInfo)
                showLobby(lobby)
                if(activeGames != null){
                    showActiveGames(activeGames)
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            activity?.runOnUiThread {
                binding.serverInfo.text = getMessage(e)
            }
        }
    }

    private fun getMessage(e: Exception) = if(e is NotFoundException) getString(R.string.serverOffline) else e.message

    private fun showInfo(info: ServerInfo){
        binding.serverInfo.text = getString(R.string.serverStatusMessage, info.playerCount, info.gameCount)
        announcementAdapter.updateList(info.announcements)
    }

    private fun showLobby(requestLobby: RequestLobby){
        binding.requestsParent.removeAllViews()
        requestLobby.requests.forEach { request: GameRequest ->
            val requestView = GameRequestView(requireActivity(), binding.requestsParent, request, this)
            binding.requestsParent.addView(requestView.getView())
        }
    }

    private fun showActiveGames(games: Games){
        binding.homeActiveGames.adapter = GameAdapter(games.games.toMutableList(), this)
    }

    override fun onClick(configuration: GameConfiguration) {
        if(checkCredentials()){
            if(configuration.isCustomGame()){
                // User clicked on custom game
                CustomGameConfigurationDialog(requireActivity(), this)
            }else{
                GameRequestAddDialog(requireActivity(), configuration)
            }
        }
    }

    override fun onChoose(configuration: GameConfiguration) {
        onClick(configuration)
    }

    override fun onClick(gameRequest: GameRequest) {
        if(checkCredentials()){
            GameRequestAcceptDialog(requireActivity(), gameRequest)
        }
    }

    private fun checkCredentials(): Boolean {
        return if(DataRepository.getInstance().credentialsAvailable(requireContext())){
            true
        }else{
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            false
        }
    }

    private fun checkFirstStart() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        if(prefs.getBoolean("firstStart", true)){
            prefs.edit().putBoolean("firstStart", false).apply()
            showTutorialDialog()
        }
    }

    private fun showTutorialDialog() {
        AlertDialog.Builder(activity)
                .setTitle(R.string.tutorialTitle)
                .setMessage(R.string.tutorialQuestion)
                .setPositiveButton(R.string.yes) { _: DialogInterface, _: Int -> openTutorial() }
                .setNegativeButton(R.string.no, null)
                .show()
    }

    private fun openTutorial() {
        findNavController().navigate(R.id.action_nav_home_to_nav_tutorial)
    }

    override fun onClick(gameInfo: GameInfo) {
        val bundle = Bundle()
        bundle.putLong(OnlineGameFragment.EXTRA_GAME_ID, gameInfo.gameId)
        findNavController().navigate(R.id.action_nav_home_to_nav_game, bundle)
    }

    override fun onUrlClick(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        activity?.startActivity(intent)
    }

}