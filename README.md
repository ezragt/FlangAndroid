# Flang Android

[Flang](https://codeberg.org/jannis/Flang) is a board game similar to chess or shogi but differs in some special [rules](https://codeberg.org/jannis/Flang/wiki/Rules).

## Screenshots

<img src="https://codeberg.org/jannis/FlangAndroid/raw/branch/master/doc/screenshots/screen1.png" width="300"/>
<img src="https://codeberg.org/jannis/FlangAndroid/raw/branch/master/doc/screenshots/screen2.png" width="300"/>
<img src="https://codeberg.org/jannis/FlangAndroid/raw/branch/master/doc/screenshots/screen3.png" width="300"/>
<img src="https://codeberg.org/jannis/FlangAndroid/raw/branch/master/doc/screenshots/screen4.png" width="300"/>

## Client Features

- Play Flang: offline and online
- Flang TV: see other people play Flang
- Profile: watch played games
- [Analysis board](https://flang.tadris.de/showthread.php?tid=4): reinvestigate your games, optionally with computer hints
- Tutorial

## Forum and discussion

There is a [forum page](https://flang.tadris.de/)! You can ask questions there, help other players understanding the game, publish your new opening or strategies that you discovered.

Join the bridged chat on:

- Matrix: [#flang:matrix.org](https://matrix.to/#/#flang:matrix.org)
- Telegram: [@flanggame](https://t.me/flanggame)

## Notices

A HUGE thank you to [Lichess](https://lichess.org/) which is licensed under the GNU GPLv3 just like Flang.
Flang uses several assets such as piece images and game sounds from the Lichess repository in the Android client and I hope it's okay to use them.
If not, please mail me (address below).

You can find more copyright notices [here](https://codeberg.org/jannis/FlangAndroid/src/branch/master/NOTICE.md).

## Donating

You want to donate? Here are some options:

<a href="https://liberapay.com/jannis/donate">
    <img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg">
</a>

* [Liberapay](https://liberapay.com/jannis/donate) (via PayPal)
* BTC: 3KDMLFrcDjeuXBM1CFpkHYBZLr23jF4JCw

You can also send a [mail](mailto:jannis@tadris.de) to get in touch with me.

## License

Copyright (C) 2021 Jannis Scheibe <jannis@tadris.de>

	Flang is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Flang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Flang ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Flang wird in der Hoffnung bereitgestellt, dass es nützlich sein wird, jedoch
    OHNE JEDE GEWÄHR,; sogar ohne die implizite
    Gewähr der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.